package com.doris.Registration;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;



import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Home extends JFrame {

	private JPanel contentPane;
	private static JTextField textField;
	private static JTextField textField_1;
	private static JTextField textField_2;
	private static JTextField textField_3;
	private static JPasswordField passwordField;
	private JLabel lblHome;

	/**
	 * Launch the application.
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home frame = new Home();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});	  
	}

	      
		
	

	/**
	 * Create the frame.
	 */
	public Home() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(23, 43, 46, 14);
		contentPane.add(lblName);
		
		textField = new JTextField();
		textField.setBounds(108, 40, 142, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblBirthdate = new JLabel("BirthDate");
		lblBirthdate.setBounds(23, 75, 46, 14);
		contentPane.add(lblBirthdate);
		
		textField_1 = new JTextField();
		textField_1.setBounds(108, 72, 142, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setBounds(23, 106, 46, 14);
		contentPane.add(lblGender);
		
		textField_2 = new JTextField();
		textField_2.setBounds(108, 103, 142, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblPhonenumber = new JLabel("PhoneNumber");
		lblPhonenumber.setBounds(23, 151, 73, 14);
		contentPane.add(lblPhonenumber);
		
		textField_3 = new JTextField();
		textField_3.setBounds(108, 148, 142, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblPasssword = new JLabel("PasssWord");
		lblPasssword.setBounds(23, 194, 73, 14);
		contentPane.add(lblPasssword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(108, 191, 142, 20);
		contentPane.add(passwordField);
		
		JButton btnMore = new JButton("Information");
		btnMore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					information();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}

			

		});
		btnMore.setBounds(118, 228, 89, 23);
		contentPane.add(btnMore);
		
		lblHome = new JLabel("HOME");
		lblHome.setBounds(149, 15, 46, 14);
		contentPane.add(lblHome);
	}
	

	private static Connection getConnection() {
		try {
	        Connection con = DriverManager.getConnection
	        ("jdbc:mysql://localhost:3306/test","classstudent",""); 
	        return con;
		}
	    catch(SQLException e){
	       System.out.println("SQL exception occured" + e);
	    }
		return null;
	}

	private static void getDBInstance() {
		 try {
	         Class.forName("com.mysql.jdbc.Driver");
	      }
	      catch(ClassNotFoundException e) {
	         System.out.println("Class not found "+ e);
	      }
		 System.out.println("JDBC Class found");
	}
	public  void information() throws IOException {
		
		getDBInstance();
		   Connection con= getConnection();
	  
	   try {
	     
	      Statement stmt = con.createStatement();
	      ResultSet rs = stmt.executeQuery
	      ("SELECT * FROM student");
	     while (rs.next()) {
	         int id = rs.getInt("id"); 
	         String Name = rs.getString("name");
	         String BirthDate = rs.getString("BirthDate");
	         String Gender = rs.getString("Gender");
	         String PhoneNumber = rs.getString("PhoneNumber");
	         String PassWord = rs.getString("PassWord");
	         System.out.println(Name+"   "+BirthDate+"    "+Gender+" "+PhoneNumber+" "+" "+PassWord);
	         ((Appendable) textField).append(Name);
	         ((Appendable) textField_1).append(BirthDate);
	         ((Appendable) textField_2).append(Gender);
	         ((Appendable) textField_3).append(PhoneNumber);
	         ((Appendable) passwordField).append(PassWord);
	         
	      }
	   
	   }
	   catch(SQLException e){
		         System.out.println("SQL exception occured" + e);
		      }
			
	}
	
}
